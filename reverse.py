def main():
    with open('citat.txt', 'r') as file:
        with open('rezultat.txt', 'w') as upis:
            content = file.read()
            sentences = content.split(".")

            for sentence in sentences:
                if sentences.index(sentence) % 2 == 1:
                    reversed_text = reverse_text(sentence)
                    upis.write(reversed_text + " ")
                else:
                    upis.write(sentence + " ")


def reverse_text(sentence):
    reversed_order = sentence.split()
    reversed_order.reverse()
    return " ".join(reversed_order)


if __name__ == "__main__":
    main()
